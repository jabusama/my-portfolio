import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './global.scss';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle';
import 'react-responsive-carousel/lib/styles/carousel.min.css';

import AOS from 'aos';
import 'aos/dist/aos.css';

AOS.init();

ReactDOM.render(<App />, document.getElementById('root'));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
