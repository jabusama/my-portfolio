import React, { useEffect, useState } from 'react';
import { Carousel } from 'react-responsive-carousel';

import { Button, Modal } from 'react-bootstrap';

import Pic1 from '../../img/portfolio/portfolio-1.jpg';
import Pic2 from '../../img/portfolio/portfolio-2.jpg';
import Pic3 from '../../img/portfolio/portfolio-3.jpg';

import { ProjectDetail } from '../../Types';

import './style.scss';

interface Props {
  isOpen: boolean;
  closeModal: () => void;
  data: ProjectDetail;
}

const Index: React.FC<Props> = (props) => {
  const { isOpen, closeModal, data } = props;

  return (
    <>
      <Modal show={isOpen} onHide={closeModal} centered size='xl'>
        <Modal.Body className='p-5' data-aos='fade-up' data-aos-delay={100}>
          <div className='row'>
            <div className='col-md-5'>
              <Carousel
                showArrows={false}
                showThumbs={false}
                showStatus={false}
                autoPlay={true}
                swipeable={true}
                emulateTouch={true}
              >
                <img src={Pic1} alt='Tour Photo' className='' />
                <img src={Pic2} alt='Tour Photo' className='' />
                <img src={Pic3} alt='Tour Photo' className='' />
              </Carousel>
            </div>
            <div className='col-md-7'>
              <a className='modal__close' onClick={closeModal}>
                &times;
              </a>

              <h2 className='heading-secondary u-margin-bottom-small mb-3'>
                {data.projectName}
              </h2>
              <h3 className='heading-teritiary u-margin-bottom-small'>
                Project Information
              </h3>

              <ul className='mb-3'>
                <li>
                  <p className='paragraph'>
                    <strong>Category:</strong> {data.category}
                  </p>
                </li>
                <li>
                  <p className='paragraph'>
                    <strong>Client:</strong> {data.client}
                  </p>
                </li>
                <li>
                  <p className='paragraph'>
                    <strong>Project URL:</strong> {data.siteURL}
                  </p>
                </li>
                <li>
                  <p className='paragraph'>
                    <strong>Technology Used:</strong> {data.tech.join()}
                  </p>
                </li>
              </ul>
              <p className='paragraph'>{data.description}</p>
              <div style={{ display: 'flex', justifyContent: 'center' }}>
                <a
                  href={data.siteURL}
                  target='_blank'
                  className='btn btn--blue me-3'
                >
                  View OPTX Site
                </a>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default Index;
