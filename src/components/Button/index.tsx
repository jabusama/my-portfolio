import React from 'react';
import './style.scss';

interface Props {
  className: string;
  action?: () => void;
}

const Button: React.FC<Props> = (props) => {
  const { className, children, action } = props;
  return (
    <a className={className} onClick={action}>
      {children}
    </a>
  );
};

export default Button;
