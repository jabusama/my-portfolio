import React, { useEffect, useState } from 'react';
import './style.scss';

import { sectionTypes } from '../../sections/Home';
import db from '../../db.json';

interface Props {}

const initValue = {
  hero: false,
  about: false,
  skills: false,
  resume: false,
  portfolio: false,
  contact: false,
};

const Nav: React.FC<Props> = () => {
  const navigationList = db.navigation;
  const [section, setSection] = useState<sectionTypes | any>(initValue);
  const [navMenuState, setNavMenuState] = useState(false);

  useEffect(() => {
    const onScroll = () => {
      if (window.pageYOffset <= 350 || window.pageYOffset === 0) {
        setSection({ ...initValue, hero: true });
      } else if (window.pageYOffset <= 950 && window.pageYOffset >= 500) {
        setSection({ ...initValue, about: true });
      } else if (window.pageYOffset <= 1600 && window.pageYOffset >= 1150) {
        setSection({ ...initValue, skills: true });
      } else if (window.pageYOffset <= 2350 && window.pageYOffset >= 1800) {
        setSection({ ...initValue, resume: true });
      } else if (window.pageYOffset <= 3100 && window.pageYOffset >= 2700) {
        setSection({ ...initValue, portfolio: true });
      } else if (window.pageYOffset >= 3400) {
        setSection({ ...initValue, contact: true });
      }
    };

    onScroll();
    window.addEventListener('scroll', onScroll);
    return () => window.removeEventListener('scroll', onScroll);
  }, []);

  useEffect(() => {
    if (navMenuState) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = 'scroll';
    }
  }, [navMenuState]);

  useEffect(() => {
    const onResize = () => {
      if (window.innerWidth > 992) {
        setNavMenuState(false);
      }
    };

    window.addEventListener('resize', onResize);
    return () => window.removeEventListener('resize', onResize);
  }, []);

  return (
    <React.Fragment>
      <div
        className={`navigation__drawer ${
          navMenuState ? 'navigation__drawer-open' : ''
        }`}
      >
        <div
          className='navigation__drawer-mask'
          onClick={() => setNavMenuState(false)}
        ></div>
      </div>

      <header
        className={`d-flex flex-column justify-content-center navigation ${
          navMenuState && 'navigation__mobile-active'
        }`}
        id='header'
      >
        <label
          className='navigation__button'
          onClick={() => setNavMenuState(!navMenuState)}
        >
          <span className='navigation__icon'>&nbsp;</span>
        </label>
        <nav className='navigation__nav'>
          <ul className='navigation__list'>
            {navigationList.map((item, i) => (
              <li
                className={`navigation__item ${section[item.id] && 'active'}`}
                key={i}
              >
                <a href={`#${item.id}`} className='navigation__link'>
                  <i className={`bx bx-${item.icon}`}></i>
                  <span>{item.name}</span>
                </a>
              </li>
            ))}
          </ul>
        </nav>
      </header>
    </React.Fragment>
  );
};

export default Nav;
