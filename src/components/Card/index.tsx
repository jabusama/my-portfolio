import React, { useState } from 'react';
import './_style.scss';

import Button from '../Button';
import Modal from '../Modal';

import { ProjectDetail } from '../../Types';

interface Props {
  projects: ProjectDetail[];
}

const initProject: ProjectDetail = {
  id: 0,
  projectName: '',
  tech: [],
  frontClass: '',
  headingClass: '',
  backClass: '',
  description: '',
  category: '',
  client: '',
  siteURL: '',
};

const Card: React.FC<Props> = (props) => {
  const { projects } = props;

  const [openModal, setOpenModal] = useState(false);
  const [selectedProject, setSelectedProject] = useState<ProjectDetail>(
    initProject
  );

  const closeModal = () => {
    setOpenModal(false);
  };

  const showModal = (id: number) => {
    projects.map((item: ProjectDetail) => {
      if (item.id === id) {
        setSelectedProject(item);
      }
    });
    setOpenModal(true);
  };

  return (
    <div className='row'>
      <Modal
        isOpen={openModal}
        closeModal={closeModal}
        data={selectedProject}
      />
      {projects.map((project: ProjectDetail, index: number) => (
        <div className='col-lg-4 col-md-6' key={index}>
          <div className='card'>
            <div className='card__side card__side--front'>
              <div className={`card__picture ${project.frontClass}`}>
                &nbsp;
              </div>
              <h4 className='card__heading'>
                <span className={`card__heading-span ${project.headingClass}`}>
                  {project.projectName}
                </span>
              </h4>
              <div className='card__details'>
                <div className='heading-teritiary'>Tecknology Used</div>
                <ul>
                  {project.tech.map((item: string, i: number) => (
                    <li key={i}>{item}</li>
                  ))}
                </ul>
              </div>
            </div>
            <div className={`card__side card__side--back ${project.backClass}`}>
              <div className='card__cta'>
                <div className='card__price-box'>
                  <p className='card__price-only'>Explore</p>
                  <p className='card__price-value'>
                    {project.projectName.toUpperCase()}
                  </p>
                </div>

                <Button
                  className='btn btn--white'
                  action={() => showModal(project.id)}
                >
                  View Details
                </Button>
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Card;
