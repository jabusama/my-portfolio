import React, { useEffect, useState } from 'react';

interface Props {
  name: string;
  value: number;
  // ref: React.MutableRefObject<null>;
  start: boolean;
}

const ProgressBars: React.FC<Props> = (props) => {
  const { name, value, start = false } = props;

  const [initValue, setInitValue] = useState(0);

  useEffect(() => {
    if (start) {
      setInitValue(value);
    }
  }, [start]);

  return (
    <div className='progress mb-2'>
      <span className='paragraph--sub skill'>
        {name} <i className='val'>{value}%</i>
      </span>
      <div className='progress-bar-wrap'>
        <div
          className='progress-bar'
          role='progressbar'
          style={{ width: `${initValue}%` }}
          aria-valuenow={initValue}
          aria-valuemin={0}
          aria-valuemax={100}
        ></div>
      </div>
    </div>
  );
};

export default ProgressBars;
