import React from 'react';

interface Props {
  image: any;
  name: string;
  category: string;
}

const PorfolioItem: React.FC<Props> = (props) => {
  const { image, name, category } = props;
  return (
    <div
      className={`col-lg-4 col-md-6 portfolio-item filter-${category.toLocaleLowerCase()}`}
    >
      <div className='portfolio-wrap'>
        <img src={image} className='img-fluid' alt='' />
        <div className='portfolio-info'>
          <h4>{name}</h4>
          <p>{category}</p>
          <div className='portfolio-links'>
            <a
              href={image}
              data-gall='portfolioGallery'
              className='venobox'
              title='App 1'
            >
              <i className='bx bx-plus'></i>
            </a>
            <a
              href='portfolio-details.html'
              data-gall='portfolioDetailsGallery'
              data-vbtype='iframe'
              className='venobox'
              title='Portfolio Details'
            >
              <i className='bx bx-link'></i>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PorfolioItem;
