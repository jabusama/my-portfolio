import React from 'react';
import PortfolioItem from './PorfolioItem';

import { PortfolioTypes } from '../../sections/Portfolio/';

interface Props {
  data: PortfolioTypes[];
}

const PorfolioList: React.FC<Props> = (props) => {
  const { data } = props;
  return (
    <div
      className='row portfolio-container'
      data-aos='fade-up'
      data-aos-delay='200'
    >
      {data.map((item, i) => (
        <PortfolioItem
          key={i}
          image={item.image}
          name={item.name}
          category={item.category}
        />
      ))}
    </div>
  );
};

export default PorfolioList;
