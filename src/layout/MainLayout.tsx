import React, { useEffect } from 'react';
import Nav from '../components/Nav';
import Footer from '../sections/Footer';

import { sectionTypes } from '../sections/Home';

interface Props {
  children: React.ReactNode;
}
const MainLayout: React.FC<Props> = (props) => {
  const { children } = props;

  // useEffect(() => {}, [currentSection]);
  return (
    <div>
      <Nav />
      {children}
      <Footer />
    </div>
  );
};

export default MainLayout;
