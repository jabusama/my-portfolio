import Home from '../sections/Home';

const routes = [{ path: '/', name: 'Home', key: 'home', component: Home }];

export default routes;
