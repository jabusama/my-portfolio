import React from 'react';
import Button from '../../components/Button';

import './style.scss';

interface Props {
  innerRef: React.ForwardedRef<any>;
}

const Contact: React.FC<Props> = (props) => {
  const { innerRef } = props;
  return (
    <section id='contact' className='contact'>
      <div className='container' data-aos='fade-up'>
        <div className='section-title'>
          <h2 className='heading-secondary'>Contact</h2>
        </div>

        <div className='row mt-1' ref={innerRef}>
          <div className='col-lg-4'>
            <div className='info'>
              <div className='address'>
                <i className='icofont-google-map'></i>
                <h4>Location:</h4>
                <p>Brgy. Pob. 21C, Jacinto Street, Davao City</p>
              </div>

              <div className='email'>
                <i className='icofont-envelope'></i>
                <h4>Email:</h4>
                <p>jepoymabusama16@gmail.com</p>
              </div>

              <div className='phone'>
                <i className='icofont-phone'></i>
                <h4>Call:</h4>
                <p>+639756523938</p>
              </div>
            </div>
          </div>

          <div className='col-lg-8 mt-5 mt-lg-0'>
            <form className='contact-email-form'>
              <div className='row'>
                <div className='col-md-6 form-group'>
                  <input
                    type='text'
                    name='name'
                    className='form-control'
                    id='name'
                    placeholder='Your Name'
                    data-rule='minlen:4'
                    data-msg='Please enter at least 4 chars'
                  />
                  <div className='validate'></div>
                </div>
                <div className='col-md-6 form-group mt-3 mt-md-0'>
                  <input
                    type='email'
                    className='form-control'
                    name='email'
                    id='email'
                    placeholder='Your Email'
                    data-rule='email'
                    data-msg='Please enter a valid email'
                  />
                  <div className='validate'></div>
                </div>
              </div>
              <div className='form-group mt-3'>
                <input
                  type='text'
                  className='form-control'
                  name='subject'
                  id='subject'
                  placeholder='Subject'
                  data-rule='minlen:4'
                  data-msg='Please enter at least 8 chars of subject'
                />
                <div className='validate'></div>
              </div>
              <div className='form-group mt-3'>
                <textarea
                  className='form-control'
                  name='message'
                  rows={5}
                  data-rule='required'
                  data-msg='Please write something for us'
                  placeholder='Message'
                ></textarea>
                <div className='validate'></div>
              </div>
              <div className='mb-3'>
                <div className='loading'>Loading</div>
                <div className='error-message'></div>
                <div className='sent-message'>
                  Your message has been sent. Thank you!
                </div>
              </div>
              <div className='text-center'>
                <Button className='btn btn--blue'>Send Message</Button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Contact;
