import React, { useState } from 'react';
import Layout from '../../layout/MainLayout';
import Typed from 'react-typed';

import About from '../About';
import Skills from '../Skills';
import Resume from '../Resume';
import Porfolio from '../Portfolio';
import Contact from '../Contact';

import { Waypoint } from 'react-waypoint';

export interface sectionTypes {
  hero: boolean;
  about: boolean;
  skills: boolean;
  resume: boolean;
  porfolio: boolean;
  contact: boolean;
}

const initValue = {
  hero: false,
  about: false,
  skills: false,
  resume: false,
  porfolio: false,
  contact: false,
};

const Home = React.forwardRef((props, ref) => {
  const [section, setSection] = useState(1);
  const [prevSection, setPrevSection] = useState(1);

  const [prevState, setPrevState] = useState<sectionTypes>(initValue);

  const onEnter = (sectionNumber: number) => {
    setSection(sectionNumber);
  };

  const onLeave = (sectionName: string) => {
    // setPrevSection(num);
    setPrevState({ ...initValue, [sectionName]: true });
  };
  return (
    <Layout>
      <section id='hero' className='d-flex flex-column justify-content-center'>
        <div className='row'>
          <div className='col-lg-4'></div>
          <div className='col-lg-8'>
            <div className='container' data-aos='zoom-in' data-aos-delay='100'>
              <h1>Jepoy Abusama</h1>
              <p>
                I'm{' '}
                <Typed
                  strings={[
                    'Software Developer',
                    'Freelancer',
                    'Mobile Developer',
                  ]}
                  typeSpeed={60}
                  backSpeed={60}
                  loop
                ></Typed>
              </p>
              <div className='social-links'>
                <a href='#about' className='gmail'>
                  <i className='bx bxl-google'></i>
                </a>
                <a
                  href='https://www.facebook.com/japoyabusama'
                  target='_blank'
                  className='facebook'
                >
                  <i className='bx bxl-facebook'></i>
                </a>

                <a href='#about' className='google-plus'>
                  <i className='bx bxl-skype'></i>
                </a>
                <a
                  href='https://www.linkedin.com/in/jepoy-abusama-39294414a/'
                  target='_blank'
                  className='linkedin'
                >
                  <i className='bx bxl-linkedin'></i>
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>

      <main id='main'>
        <section id='section' className='section'>
          <About innerRef={ref} />

          <Skills innerRef={ref} />

          <Resume innerRef={ref} />

          <Porfolio innerRef={ref} />

          <Contact innerRef={ref} />
        </section>
      </main>
    </Layout>
  );
});

export default Home;
