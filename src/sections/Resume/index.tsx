import React from 'react';
import './style.scss';
import db from '../../db.json';

interface Props {
  innerRef: React.ForwardedRef<any>;
}

const Resume: React.FC<Props> = (props) => {
  const { innerRef } = props;

  const resumeData = db.resume.map((item) => item);

  return (
    <section id='resume' className='resume'>
      <div className='container' data-aos='fade-up'>
        <div className='section-title'>
          <h2 className='heading-secondary' ref={innerRef}>
            Resume
          </h2>
          {/* <p className='paragraph'>
            Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex
            aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos
            quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia
            fugiat sit in iste officiis commodi quidem hic quas.
          </p> */}
        </div>

        <div className='row'>
          {resumeData.map((resumeColumn, key) => (
            <div className={'col-lg-6'} key={key}>
              {resumeColumn.column.map((item, i) => (
                <React.Fragment key={i}>
                  <h3 className='heading-secondary--sub mb-3' key={i}>
                    {item.name}
                  </h3>
                  {item.data.map((list, i) => (
                    <div className='resume-item pb-0' key={i}>
                      <h4 className='heading-teritiary'>{list.title}</h4>
                      {list.duration !== '' && (
                        <h5 className='heading-teritiary--sub'>
                          {list.duration}
                        </h5>
                      )}

                      {list.institution !== '' && (
                        <p className='paragraph'>
                          <em>{list.institution}</em>
                        </p>
                      )}

                      {list.description !== '' && (
                        <p className='paragraph--sub'>
                          <em>{list.description}</em>
                        </p>
                      )}

                      {list.roles.length !== 0 && (
                        <ul className='paragraph--sub'>
                          {list.roles.map((role, i) => (
                            <li key={i}>{role}</li>
                          ))}
                        </ul>
                      )}
                    </div>
                  ))}
                </React.Fragment>
              ))}
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};

export default Resume;
