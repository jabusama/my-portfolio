import React from 'react';
import './style.scss';
import { Waypoint } from 'react-waypoint';
import MyProfile from '../../img/profile.jpg';

interface Props {
  innerRef: React.ForwardedRef<any>;
}

const About: React.FC<Props> = (props) => {
  const { innerRef } = props;

  return (
    <section id='about' className='about'>
      <div className='container' data-aos='fade-up'>
        <div className='section-title'>
          <h2 className='heading-secondary mb-3' ref={innerRef}>
            About
          </h2>
          <p className='paragraph'>
            Over 3+ years experience as a Software developer and have worked for
            both small startups and large organizations. While I am proficient
            in Front End and Backend Development. My expertise is in building
            advanced scalable UI/UX design and structure.
          </p>
        </div>
        <div className='row'>
          <div className='col-lg-4'>
            <img src={MyProfile} className='img-fluid' alt='' />
          </div>
          <div className='col-lg-8 pt-4 pt-lg-0 content'>
            <h3 className='heading-secondary--sub mb-3'>Software Developer.</h3>
            <p className='paragraph'>
              Proficient in HTML, CSS, SASS, Less and multiple Javascript
              Frameworks/Library such as ReactJS, React Native, VueJS, NodeJS,
              AdonisJS and ExpressJS. Strong understanding of CSS Frameworks
              such as Bootstrap, AntD, Vuetify and Material UI. And also had
              worked on Backend technologies such as Rest API, MonggoDB, MySQL,
              NodeJs, AdonisJs, and ExpressJs. Also familiar with different
              project management methodologies such as waterfall, agile and
              scrum agile framework.
            </p>
            <div className='row'>
              <div className='col-lg-6'>
                <ul>
                  <li>
                    <p className='paragraph'>
                      <i className='icofont-rounded-right'></i>{' '}
                      <strong>Birthday:</strong> July 19, 1996
                    </p>
                  </li>
                  <li>
                    <p className='paragraph'>
                      <i className='icofont-rounded-right'></i>{' '}
                      <strong>Degree:</strong> BS in Computer Engineering
                    </p>
                  </li>
                  <li>
                    <p className='paragraph'>
                      <i className='icofont-rounded-right'></i>{' '}
                      <strong>Phone:</strong> +639756523938/+639091727105
                    </p>
                  </li>
                  <li>
                    <p className='paragraph'>
                      <i className='icofont-rounded-right'></i>{' '}
                      <strong>City:</strong> Davao City
                    </p>
                  </li>
                </ul>
              </div>
              <div className='col-lg-6'>
                <ul>
                  <li>
                    <p className='paragraph'>
                      <i className='icofont-rounded-right'></i>{' '}
                      <strong>Age:</strong> 24
                    </p>
                  </li>
                  <li>
                    <p className='paragraph'>
                      <i className='icofont-rounded-right'></i>{' '}
                      <strong>Email:</strong>{' '}
                      <a href='mailto:jepoymabusama16@gmail.com'>
                        jepoymabusama16@gmail.com
                      </a>
                    </p>
                  </li>
                  <li>
                    <p className='paragraph'>
                      <i className='icofont-rounded-right'></i>{' '}
                      <strong>Full Time:</strong> Available
                    </p>
                  </li>
                </ul>
              </div>
            </div>
            <p className='paragraph'>
              I am looking for a senior individual contributor role where I can
              take on collaborative team, leadership responsibilities and gain
              experience with competitive software engineering and project
              management.
            </p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default About;
