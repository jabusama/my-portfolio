import React from 'react';

import Image1 from '../../img/portfolio/portfolio-1.jpg';
import Image2 from '../../img/portfolio/portfolio-2.jpg';
import Image3 from '../../img/portfolio/portfolio-3.jpg';

const PorfolioDetails = () => {
  return (
    <section id='portfolio-details' className='portfolio-details'>
      <div className='container' data-aos='fade-up'>
        <div className='row'>
          <div className='col-lg-8'>
            <h2 className='portfolio-title'>
              This is an example of portfolio detail
            </h2>
            <div className='owl-carousel portfolio-details-carousel'>
              <img src={Image1} className='img-fluid' alt='' />
              <img src={Image2} className='img-fluid' alt='' />
              <img src={Image3} className='img-fluid' alt='' />
            </div>
          </div>

          <div className='col-lg-4 portfolio-info'>
            <h3>Project information</h3>
            <ul>
              <li>
                <strong>Category</strong>: Web design
              </li>
              <li>
                <strong>Client</strong>: ASU Company
              </li>
              <li>
                <strong>Project date</strong>: 01 March, 2020
              </li>
              <li>
                <strong>Project URL</strong>: <a href='#'>www.example.com</a>
              </li>
            </ul>

            <p>
              Autem ipsum nam porro corporis rerum. Quis eos dolorem eos itaque
              inventore commodi labore quia quia. Exercitationem repudiandae
              officiis neque suscipit non officia eaque itaque enim. Voluptatem
              officia accusantium nesciunt est omnis tempora consectetur
              dignissimos. Sequi nulla at esse enim cum deserunt eius.
            </p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default PorfolioDetails;
