import React, { useEffect, useState } from 'react';
// import Isotope from 'isotope-layout/js/isotope';

import Image1 from '../../img/portfolio/portfolio-1.jpg';
import Image2 from '../../img/portfolio/portfolio-2.jpg';
import Image3 from '../../img/portfolio/portfolio-3.jpg';
import Image4 from '../../img/portfolio/portfolio-4.jpg';
import Image5 from '../../img/portfolio/portfolio-5.jpg';
import Image6 from '../../img/portfolio/portfolio-6.jpg';
import Image7 from '../../img/portfolio/portfolio-7.jpg';
import Image8 from '../../img/portfolio/portfolio-8.jpg';
import Image9 from '../../img/portfolio/portfolio-9.jpg';

import Modal from '../../components/Modal';
import PorfolioList from '../../components/PorfolioCotainer';
import Card from '../../components/Card';

import db from '../../db.json';

export interface PortfolioTypes {
  image: any;
  name: string;
  category: string;
}

const data = [
  { image: Image1, name: 'App 1', category: 'App' },
  { image: Image2, name: 'Web 1', category: 'Web' },
  { image: Image3, name: 'App 2', category: 'App' },
  { image: Image4, name: 'Card 2', category: 'Card' },
  { image: Image5, name: 'Web 3', category: 'Web' },
  { image: Image6, name: 'App 1', category: 'App' },
  { image: Image7, name: 'Card 2', category: 'Card' },
  { image: Image8, name: 'Card 3', category: 'Card' },
  { image: Image9, name: 'Web 3', category: 'Web' },
];

interface Props {
  innerRef: React.ForwardedRef<any>;
}

const Porfolio: React.FC<Props> = (props) => {
  const { innerRef } = props;
  const { porfolio } = db;

  return (
    <section id='portfolio' className='portfolio section-bg'>
      <div className='container' data-aos='fade-up'>
        <div className='section-title'>
          <h2 className='heading-secondary'>Portfolio</h2>
          <p className='paragraph'>
            List of project that I was participated in development and
            maintained during my professional experience.
          </p>
        </div>

        <div ref={innerRef}>
          <Card projects={porfolio} />
        </div>
      </div>
    </section>
  );
};

export default Porfolio;
