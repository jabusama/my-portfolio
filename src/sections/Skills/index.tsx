import React, { useState } from 'react';
import './style.scss';
import ProgressBars from '../../components/ProgressBars';
import mockData from '../../db.json';

import { Waypoint } from 'react-waypoint';

interface Props {
  innerRef: React.ForwardedRef<any>;
}

const About: React.FC<Props> = (props) => {
  const { innerRef } = props;
  const { skills: skillData = [] } = mockData;

  const [start, setStart] = useState(false);
  const onEnter = () => {
    if (!start) setStart(true);
  };

  return (
    <section id='skills' className='skills section-bg'>
      <div className='container' data-aos='fade-up'>
        <div className='section-title'>
          <h2 className='heading-secondary mb-3' ref={innerRef}>
            Skills
          </h2>
          {/* <p className='paragraph'>
            Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex
            aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos
            quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia
            fugiat sit in iste officiis commodi quidem hic quas.
          </p> */}
        </div>

        <Waypoint onEnter={() => onEnter()}>
          <div className='row skills-content'>
            {skillData.map((skill, i) => (
              <div className='col' key={i}>
                <h3 className='heading-secondary--sub'>{skill.type}</h3>
                {skill.skillTypes.map((type, i) => (
                  <ProgressBars
                    name={type.name}
                    value={type.value}
                    key={i}
                    start={start}
                  />
                ))}
              </div>
            ))}
          </div>
        </Waypoint>
      </div>
    </section>
  );
};

export default About;
