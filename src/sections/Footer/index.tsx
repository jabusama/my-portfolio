import React from 'react';
import './style.scss';

const Footer = () => {
  return (
    <footer id='footer' className='footer'>
      <div className='container'>
        <h3 className='heading-primary heading-primary--sub'>
          Jepoy Mentang Abusama
        </h3>
        <p className='paragraph'>
          <q>If you are smartest in the room you are in the room</q>
        </p>
        <div className='social-links'>
          <a href='#' className='twitter'>
            <i className='bx bxl-twitter'></i>
          </a>
          <a href='#' className='facebook'>
            <i className='bx bxl-facebook'></i>
          </a>
          <a href='#' className='instagram'>
            <i className='bx bxl-instagram'></i>
          </a>
          <a href='#' className='google-plus'>
            <i className='bx bxl-skype'></i>
          </a>
          <a href='#' className='linkedin'>
            <i className='bx bxl-linkedin'></i>
          </a>
        </div>
        <div className='copyright'>
          &copy; Copyright{' '}
          <strong>
            <span>jepoyabusama</span>
          </strong>
          . All Rights Reserved
        </div>
      </div>
    </footer>
  );
};

export default Footer;
