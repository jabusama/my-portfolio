export interface ProjectDetail {
  id: number;
  projectName: string;
  tech: string[];
  frontClass: string;
  headingClass: string;
  backClass: string;
  description: string;
  category: string;
  client: string;
  siteURL: string;
}
